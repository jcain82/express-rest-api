import { Schema } from 'mongoose';

export var UserSchema = new Schema({
    email: { type: String, unique: true },
    fname: String,
    lname: String,
    phone: String
});