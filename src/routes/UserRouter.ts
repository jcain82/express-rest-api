import {Router, Request, Response, NextFunction} from 'express';
import UserController from '../controllers/User';

export class UserRouter {
  router: Router
  userController: UserController

  /**
   * Initialize the UserRouter
   */
  constructor() {
    this.router = Router();
    this.userController = new UserController();
    this.init();
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  init() {
    this.router.get('/', this.userController.getAll);
    this.router.get('/:id', this.userController.getById)
    this.router.post('/', this.userController.create);
    this.router.post('/:id', this.userController.update);
    this.router.delete('/:id', this.userController.delete);
  }
}

// Create the UserRouter, and export its configured Express.Router
const UserRoutes = new UserRouter();
UserRoutes.init();

export default UserRoutes.router;