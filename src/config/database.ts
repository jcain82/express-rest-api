import { Mockgoose } from "mockgoose-fix";
import * as mongoose from "mongoose";

(mongoose as any).Promise = global.Promise;

if (process.env.NODE_ENV === "TEST") {

  const mockgoose = new Mockgoose(mongoose);
  mockgoose.helper.setDbVersion("3.4.3");

  mockgoose.prepareStorage().then((): void => {
    mongoose.connect("mongodb://example.com/TestingDB");
  });

} else {
  const MONGODB_CONNECTION: string = "mongodb://srv-nas:32768/test";
  mongoose.connect(MONGODB_CONNECTION);

}

export { mongoose };