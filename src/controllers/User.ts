import { Router, Request, Response, NextFunction } from 'express';
import { mongoose } from '../config/database';
import { IUser } from '../interfaces/User';
import { UserSchema } from '../schemas/User';

export class UserController {
    constructor() {
        mongoose.model<IUser>('User', UserSchema);
    }

    /**
     * GET all Users.
     */
    public async getAll(req: Request, res: Response, next: NextFunction) {
        mongoose.model<IUser>('User').find({}, function (err, blobs) {
            if (err) {
                return console.error(err);
            } else {
                res.json(blobs);
            }
        });
    }

    /**
     * Find User By ID
     * @param req 
     * @param res 
     * @param next 
     */
    public async getById(req: Request, res: Response, next: NextFunction) {
        mongoose.model<IUser>('User').findById(req.params.id, function (err, blobs) {
            if (err) {
                return console.error(err);
            } else {
                res.json(blobs);
            }
        });
    }

    /**
     * Insert new User
     * @param req 
     * @param res 
     * @param next 
     */
    public async create(req: Request, res: Response, next: NextFunction) {
        let user = <IUser>{};

        user.email = req.body.email;
        user.fname = req.body.fname;
        user.lname = req.body.lname;
        user.phone = req.body.phone;

        mongoose.model<IUser>('User').create(user, function (err, blobs) {
            if (err) {
                return console.error(err);
            } else {
                res.json(blobs);
            }
        });
    }

    /**
     * 
     * @param req 
     * @param res 
     * @param next 
     */
    public async update(req: Request, res: Response, next: NextFunction) {
        let user = <IUser>{};

        if (req.body.email) {
            user.email = req.body.email;
        }

        if (req.body.fname) {
            user.fname = req.body.fname;
        }

        if (req.body.lname) {
            user.lname = req.body.lname;
        }

        if (req.body.phone) {
            user.phone = req.body.phone;
        }

        mongoose.model<IUser>('User').updateOne({ _id: req.params.id }, user, function (err, blobs) {
            if (err) {
                return console.error(err);
            } else {
                res.json(blobs);
            }
        });
    }

    /**
     * 
     * @param req 
     * @param res 
     * @param next 
     */
    public async delete(req: Request, res: Response, next: NextFunction) {
        mongoose.model<IUser>('User').deleteOne({ _id: req.params.id }, function (err) {
            if (err) {
                return console.error(err);
            }
            else {
                res.json({ "status": "SUCCESS" });
            }
        });
    }
}

export default UserController;