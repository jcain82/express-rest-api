import { Document } from 'mongoose';

export interface IUser extends Document {
    email: String,
    fname?: String,
    lname?: String,
    phone?: String
};